import 'dart:io';

void main() {
  print(
      "Select the choice you want to parform \n1. ADD \n2. SUBTRACT\n3. MULTIPLY\n4. DIVIDE\n5. EXIT");
  print("Choice you want to enter : ");

  String? choice = stdin.readLineSync();

  switch (choice) {
    case "1":
      {
        print("Enter the value for x : ");
        int num1 = int.parse(stdin.readLineSync()!);
        print("Enter the value for y : ");
        int num2 = int.parse(stdin.readLineSync()!);
        int result = num1 + num2;
        print("Sum of the two numbers is : ");
        print(result);
        break;
      }
    case "2":
      {
        print("Enter the value for x : ");
        int? new1 = int.parse(stdin.readLineSync()!);
        print("Enter the value for y : ");
        int? new2 = int.parse(stdin.readLineSync()!);
        int result = new1 - new2;
        print("Difference of the two numbers is : ");
        print(result);
        break;
      }
    case "3":
      {
        print("Enter the value for x : ");
        int? new1 = int.parse(stdin.readLineSync()!);
        print("Enter the value for y : ");
        int? new2 = int.parse(stdin.readLineSync()!);
        int result = new1 * new2;
        print("Multiply of the two numbers is : ");
        print(result);
        break;
      }
    case "4":
      {
        print("Enter the value for x : ");
        int? new1 = int.parse(stdin.readLineSync()!);
        print("Enter the value for y : ");
        int? new2 = int.parse(stdin.readLineSync()!);
        int result = new1 ~/ new2;
        print("Divide of the two numbers is : ");
        print(result);
        break;
      }
    case "5":
      {
        break;
      }
  }
}
